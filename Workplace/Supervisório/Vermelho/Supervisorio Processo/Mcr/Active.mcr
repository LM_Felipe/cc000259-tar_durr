!===== Macro File created  01/24/02 ===============================
! Macros are lists of commands, with one command per line
! See Help or the manual for a list of commands and their parameters
!============================================================


DataLogOn CHILLER_STOP
DataLogOn CV_SPT
DataLogOn Casa_ar
DataLogOn Consumo de Gas
DataLogOn Consumo Motores 
DataLogOn Consumo_Peri�dico_�gua
DataLogOn Processo 
DataLogOn Estufas
DataLogOn Retificadores
DataLogOn Estufas_Antracite
DataLogOn Secador_Borra
DataLogOn Temp_Umid_Internas
DataLogOn Leituras_Periodicas_Processo_TTS
DataLogOn Leituras_Periodicas_Processo_TTS_ELPO
DataLogOn Leituras_Periodicas_Processo_ELPO
DataLogOn Leituras_Periodicas_Processo_ELPO_EL
DataLogOn Leituras_Periodicas_Estufas_ELPO
DataLogOn NIVEL_ELPO
DataLogOn NIVEL_TTS
DataLogOn Leituras_Periodicas_Estufas_PRIMER
DataLogOn Leituras_Periodicas_Estufas_TOP COAT
DataLogOn Historico_Temperatura_Umidade
DataLogOn Vazao Descarte DI
DataLogOn Consumo_Peri�dico_�gua
DataLogOn EQUIPAMENTOS
DataLogOn T3_CONTROLE
DataLogOn Processo_Robos
DataLogOn Registro de leitura estufas
DataLogOn RETIFICADOR_3
DerivedOn ALARMES_TC
DerivedOn SET_VALOR_PERCENTUAL
DerivedOn CONSUMO DE GAS
DerivedOn TTS_RANGES
DerivedOn ELPO_RANGES
DerivedOn TO_ALARME SET POINTS
DataLogOn Consumo_de_�gua
EVENTON CANTA_FALHA_ELPO
EVENTON CANTA_FALHA_AGUA_DI
EVENTON CANTA_FALHA_YORK
EVENTON Aviso
DataLogOn CONSUMO_AGUA_TREND
DataLogOn Estufa_Elpo_Dif
DataLogOn Estufa_Primer_Dif
DataLogOn Estufa_TopCoat_Dif
DataLogOn ST_CASA_DE_AR_HOSPITALL
DataLogOn BOMBA_OSMOSE_REVERSA_PRODUZINDO
DataLogOn TANQUE_RES_FOSFATO_DESENGRAXE
DataLogOn PM_BASE
DataLogOn PM_PRIMER
DataLogOn PM_VERNIZ
DataLogOn ESTAGIO2
DataLogOn NIVEL_TANQUE_ELPO
DataLogOn NIVEL_AGUA_FILTRADA_DI
DataLogOn �gua_DI
DataLogOn CICLO_PISTOLA_ANTCHIP
DataLogOn COUNTER_EFFECT
DerivedOn QUEIMADORES
DerivedOn BOMBA_OSMOSE_REVERSA
DataLogOn BOILLER
DataLogOn UFS
EVENTON EVE
EVENTON COUNTER_EFFECT
DataLogOn TESTE
DataLogOn CHILLER
DerivedOn TTS2_SET_VALOR_PERCENTUAL
DerivedOn TTS2_RANGES
DerivedOn VINCOMPL
DataLogOn EO2_Estufas
DataLogOn EO2_Leituras_Periodicas_Estufas_ELPO
DataLogOn EO2_MONITOR_L1
DataLogOn EO2_MONITOR_L2
DerivedOn EO2_CONSUMO DE GAS
DataLogOn EO2_Estufa_Elpo_Dif
DataLogOn PENDxSKID
DataLogOn PO2_Leituras_Periodicas_Estufas_PRIMER
DataLogOn PO2_Estufa_Primer_Dif
DerivedOn LOGIX_CONSUMO DE GAS
DataLogOn AL_CORRENTE
DataLogOn TO2_Leituras_Periodicas_Estufas_TOP COAT
DataLogOn TO2_Estufa_TopCoat_Dif
DataLogOn RETIFICADOR_3
DataLogOn TEMPE_ESTUFA_MENOR_TOLERADO
DataLogOn CHILLER_TEMPERATURA
DataLogOn BARRAMENTO
DataLogOn TEMP_ESTUFAS_AL
DataLogOn FAULT_BOILLER
DataLogOn CELULAS
DataLogOn CELULAS1
DataLogOn AL_CORRENTE