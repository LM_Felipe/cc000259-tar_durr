!===== Macro File created  01/30/01 ===============================
! Macros are lists of commands, with one command per line
! See Help or the manual for a list of commands and their parameters
!============================================================
DisplayserverOn

DataLogOn Processo

Display TopCoatOvenG1 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display TopCoatOvenG2 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display PrimerOvenG1 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display PrimerOvenG2 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display ElpoOvenG1 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display ElpoOvenG2 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_PT_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_PT_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_PT_03 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_EL_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_EL_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_BP_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_BP_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_CV_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_CV_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_CV_03 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_CW_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_ES_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_PS_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_OR_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_OR_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_TC_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_TC_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger
Display Trend_TC_03 /ZA
Trend\OldestTime = System\DateAndTimeInteger
DataLogOn RETIFICADOR_3
DataLogOn TEMPE_ESTUFA_MENOR_TOLERADO