!===== Arquivo de Macros criado  01/24/02 ===============================
! Macros s�o comandos adicionais, definidos na forma de um comando por linha
! Consulte a Ajuda ou o manual para obter uma lista de comandos e seus par�metros
!============================================================

ALARMON
 
Display Trend_PM_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger

Display Trend_PM_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger

Display Trend_PM_03 /ZA
Trend\OldestTime = System\DateAndTimeInteger

DataLogOn Lubrifica��o

DataLogOn PARADAS_PROCESSO

DataLogOn Velocidades

DataLogOn Cont_Zona Turnos

DataLogOn Casas de AR

DataLogOn TTS_SKIDS 

DataLogOn NIVEIS

DataLogOn Vagas_Sistema

DataLogOnVagas_Sistema_mud

DataLogOn ELPO_SKIDS 

DataLogOn Quantidade_Buffer

DataLogOn Troca_Cor_UR_12_18

DataLogOn TEMPERATURA_STRIPOUT_ELPO

DataLogOn TEMPERATURA_STRIPOUT_TOPCOAT

DataLogOn CONTROLE_BOILERS

DataLogOn CONT_BOILERS

DataLogOn TINTAS

DataLogOn Contadores_RETORNO_N16

DataLogOn TIMEOUT OPERACAO ANTICHIP

DataLogOn TIMEOUT_OPERACAO_ANTRACITE

DataLogOn PAINT_MIX_BASE

DataLogOn PAINT_MIX_PRIMER

DataLogOn PAINT_MIX_VERNIZ_SOL

DataLogOn ANDON_MANUT

DataLogOn CICLO_CEL3

DataLogOn P5_SOLVENTE

DataLogOn Downtime_LS_TTS

DataLogOn BOS_FLAPTOP

DataLogOn BOS_Parada_Processo

DerivedOn VELOCIDADES_AL

DerivedOn INFORMA��ES RS VIEW

DerivedOn SET_POINT_TEMP

DataLogOn RETIFIC_DataLogBarreira

EVENTON PLC_Q

EVENTON Casas de AR

EVENTON CANTA_A1

EVENTON CANTA_A2

EVENTON EVE_VERIFIC_SABADO

EVENTON EVE

Active Canta_F_1


EVENTON  DLGLOG_VAGASMUD_TRIGGER

DerivedOn RETIF_1

DerivedOn RETIF_2

DerivedOn RETIF_4

DerivedOn RETIF_3

DerivedOn RSVIEWINF\REFEICAO_TURNO1

DerivedOn REGISTRO_OPERADOR_ERROR_PROFING

DerivedOn TIME_OUT_SAIDA_ESTUFA_ELPO

DerivedOn TIME_OUT_SAIDA_ESTUFA_PRIMER

DerivedOn TIME_OUT_SAIDA_ESTUFA_TOP_COAT

DerivedOn BYPASS_LIMPA_CONTATO
