﻿v0.5
-Updated to include firmware v6.1.02 - handled port based DHCP
-revised nomenclature Zone->System Cell->Controller
-pull designated DHCP address for CVP from DCDL.  Added DHCP start address to main form.
-set VLAN ID 1 port egress/untagged masks. Private addresses no longer allow egress 
-set Allow Management Access for Port Mirroring to prevent possible lock-up if port mirroring is ever used
v0.6
-Renamed fields on main form for SNMP Trap/Log Hosts to indicate NNMI and Cisco prime.
-updated error message for SYSTEM/CONTROLLER designations.
-updated handling of DCDL with switch names using Zone/Cell nomenclature
-added tooltip to 'Switch Type' field on main form to show (aka Zone) and (aka Cell).
v0.7
-fixed import of switch type from DCDL
-recompiled help module to fix graphics links.  Updated to include DHCP Start Address descripition
v0.8
-added error check on convenience port IP import - if not filled in exception was thrown
-set build option for help file to 'content'
v0.9
-Updated DCDL import to eliminate excess lines in switch list
-Updated Status to indicate if errors were encountered when write process is complete, message coded as red
-Added EIP Interface enable checkbox (default to checked), request from GPS
-Added PTP enable checkbox (default to unchecked), request from GPS
-Added ability to save/open configurations to/from an xml file, request from GPS
-Added VLAN setting of 'Isolated' - used to set port to VLAN 200, request from GPS
-Enabled ability to select "Private" ports for system switches - VLAN 192 and routing will be put in all switches (no routing for controller expanded switches)
-default Port1 Name to 'Uplink', and Port4 Name to 'CVP'
-revised code to NOT set parameter for 'Allow Management Access for Port Mirroring' on Firmware 05.0.04 - Feature not preset was causing SNMP error
-fixed port monitoring interface action mode to be set to 'send trap' for all ports
-updated help documentation
-updated main form to alleviate display issues when screen settings are at 125%
-skip writing the CVP DHCP address if it is set to 0.0.0.0
-added firmware revision 06.01.04
-added ProfiNet checkbox to block setting the parameter to only send unknownmulticasts to query ports only (leave at default - flood)
v1.0.0
-Added ability to set DHCP gateway address when switch type is set to 'Controller Expanded'.  Import from DCDL will populate with value for convenience port gateway address.
 If switch type is not of the 'Expanded' variety, default gateway to public routing interface.
-revised default xml save/open file extension to .oc2
-updated help file to include description of added fields
v1.0.1
-Updated XML Open to check file format
-allow 0.0.0.0 for GPS convenience port address
v1.0.2
-added user warning message for firmware selection prior to 06.1.04
v1.0.4
-updated port egress for VLAN1 to untagged for all ports designated either as either "Public" or "Both"
-Disable ICMP Redirects for routing interfaces
-Updated configuration process to include steps for user to run additional function to clear the temporary IP info and save to NVM via eTool
-Updated PTP configuration to default 'Uplink' and 'Downlink' ports to disable
-added subnet mask selection for public routing interface.  Defaults to 255.255.255.0, user can select 255.255.254.0
-Set SNMP SET/GET logging severity to 'Warning'
-eTool configuration text added to CLI Login Banner text field
-fixed link speed/duplex port monitoring to allow for only 100FDX
-updated SNMP/SNTP server configuration to allow 0.0.0.0 to supress trying to set the entries
-updated security status monitoring selections
-updated setting of hi-discovery to read-only.  Added command to enable protocol within same SNMP packet.
-added 'Tools' menu with options to set PC interface IP and separate save to NVM function
-removed creation of loopback interface
-added 'Save as Default' option under Tools menu.  Allows user to save the current form settings as the default values to be used when the form is loaded or reset. 
 Note: Port configurations are not saved as default values.
-added configuration of password minimum length field to 8
-enable user to change the convenience port DHCP Gateway address regardless of switch type selection.  Previously only available for controller expanded switches
v1.0.5
-updated DCDL import to check for new descriptions "Uplink to Controller" or "Downlink to Controller Expanded" to set VLAN type to "Both".  This is used to set trunking masks.
v1.0.6
-updated setting of static routing entry for "super" nets with mask of 255.255.254.0 to use "even" 3rd octet
-updated snooping VLAN querier address for VLAN 1 for "super" nets with mask of 255.255.254.0 to use "even" 3rd octet
-enabled console logging
-enabled CLI command logging
-disabled SNMP GET logging
-Turned off Spanning Tree Protocol for identified CVP
-added pop-up window at end of config to remind user to set the PC IP and clear the switch temporary IP address


